"""
Green Frog

This is an object place in character generation rooms. This supplies all
of the necessary commands for creating a new character.

This should never be created in a public area.
"""

from ev import Object

from game.gamesrc.commands.frogs.cmdset import GreenFrogCmdSet

class GreenFrog(Object):
    def at_object_creation(self):
        self.cmdset.add_default(GreenFrogCmdSet, permanent=True)
