"""
This file contains the CmdSet objects for the frogs.
"""

from ev import CmdSet

from game.gamesrc.commands.frogs.commands import *

class BlueFrogCmdSet(CmdSet):

    priority = 10
    mergetype = "Union"

    def at_cmdset_creation(self):
        self.add(CmdBlueFrogLook())

class GreenFrogCmdSet(CmdSet):

    priority = 10
    mergetype = "Union"

    def at_cmdset_creation(self):
        pass

