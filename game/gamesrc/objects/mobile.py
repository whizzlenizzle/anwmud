"""
This is the base object for any Mobile in the game. This includes Characters
as well as Mobs and NPCs, or whatever else would be considered a "Mobile".
"""

from ev import Object

from game.gamesrc.objects.race import Race
from game.gamesrc.utils.stat import Stat

class Mobile(Object):
    """
    Begin the definition of the Mobile object.

    A Mobile object will have the following attributes:
        short_desc - a short description of the object
        long_desc - a long description of the object
        gender
        race
        ham - health/action/mind, typical resource pools
        stat - a dictionary of Stat objects
        professions - not implemented
        skills - not implemented
    """
    def basetype_setup(self):
        """
        Set up mobile-specific security.

        You should not need to overload this, but if you do you either
        need to super it as seen below so you include all of this, or include
        most of the elements below or you will change how Mobile objects
        fundamentally work and are structured.
        """
        # call Object's basetype_setup()
        super(Mobile, self).basetype_setup()
        # modify/add the following locks
        self.locks.add(";".join(["get:false()"]))

        #
        # Mobile Attributes
        #

        self.db.short_desc = ""
        self.db.long_desc = ""

        self.db.gender = None
        self.db.race = None

        self.db.ham = {
            'health': Stat('health'),
            'action': Stat('action'),
            'mind': Stat('mind')
        }

        self.db.stats = {
            'intelligence': Stat('intelligence', static=True),
            'wisdom': Stat('wisdom', static=True),
            'strength': Stat('strength', static=True),
            'dexterity': Stat('dexterity', static=True),
            'constitution': Stat('constitution', static=True)
        }

        self.db.professions = []
        self.db.skills = {}

    def at_object_creation(self):
        """
        This is where you will overload and supply additional attributes
        to your various types of Mobile objects.
        """
        pass

    def at_pre_puppet(self, player, sessid=None):
        """
        This is called just before the Mobile is puppeted. This will
        likely only be used for Characters as they are logging in.
        """
        pass

    def at_post_puppet(self):
        """
        Called just after puppeting is complete.
        """
        self.msg("\nYou become {y%s{n.\n" % self.name)

    def at_post_unpuppet(self, player, sessid=None):
        """
        Called just after the Mobile is unpuppeted
        """
        pass


