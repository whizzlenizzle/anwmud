from src.typeclasses.typeclass import TypeClass

__all__ = ("Profession")

_GA = object.__getattribute__
_SA = object.__setattr__
_DA = object.__delattr__

#
# Base class to inherit from.
#

class Profession(TypeClass):
    def __init__(self, dbobj):
        """
        This is the root typeclass object, representing all entities
        that have an actual presence in-game. Objects generally have a
        location. They can also be manipulated and looked at. Most
        game entities you define should inherit from Object at some distance.
        Evennia defines some important subclasses of Object by default, namely
        Characters, Exits and Rooms (see the bottom of this module).

        Note that all new Objects and their subclasses *must* always be
        created using the ev.create_object() function. This is so the
        typeclass system can be correctly initiated behind the scenes.


        Object Typeclass API:

        * Available properties (only available on *initiated* typeclass objects)

         key (string) - name of object
         name (string) - same as key
         dbref (int, read-only) - unique #id-number. Also "id" can be used.
         dbobj (Object, read-only) - link to database model. dbobj.typeclass
                     points back to this class
         typeclass (Object, read-only) - this links back to this class as an
                     identified only. Use self.swap_typeclass() to switch.
         date_created (string) - time stamp of object creation
         permissions (list of strings) - list of permission strings

         locked (bool) - if this profession object is enabled or not


        * Handlers available

         locks - lock-handler: use locks.add() to add new lock strings
         db - attribute-handler: store/retrieve database attributes on this
                                 self.db.myattr=val, val=self.db.myattr
         ndb - non-persistent attribute handler: same as db but does not
                                 create a database entry when storing data
         scripts - script-handler. Add new scripts to object with scripts.add()
         cmdset - cmdset-handler. Use cmdset.add() to add new cmdsets to object

        * Helper methods

         is_typeclass(typeclass, exact=False)
         swap_typeclass(new_typeclass, clean_attributes=False, no_default=True)
         access(accessing_obj, access_type='read', default=False)
         check_permstring(permstring)

         basetype_setup(self)
         basetype_posthook_setup(self)
         at_profession_object_creation(self)
         at_profession_object_delete(self)
         at_init(self)
         at_cmdset_get(self, **kwargs)
         at_pre_learn(self, character, proficiency)
         at_post_learn(self, character, proficiency)
         at_pre_forget(self, character, proficiency)
         at_post_forget(self, character, proficiency)
         """
        super(Profession, self).__init__(dbobj)

    def is_typeclass(self, typeclass, exact=False):
        """
        Returns true if this object has this type
          OR has a typeclass which is an subclass of
          the given typeclass.

        typeclass - can be a class object or the
                python path to such an object to match against.

        exact - returns true only if the object's
               type is exactly this typeclass, ignoring
               parents.

        Returns: Boolean
        """
        return self.dbobj.is_typeclass(typeclass, exact=exact)

    def swap_typeclass(self, new_typeclass, clean_attributes=False, no_default=True):
        """
        This performs an in-situ swap of the typeclass. This means
        that in-game, this object will suddenly be something else.
        Player will not be affected. To 'move' a player to a different
        object entirely (while retaining this object's type), use
        self.player.swap_object().

        Note that this might be an error prone operation if the
        old/new typeclass was heavily customized - your code
        might expect one and not the other, so be careful to
        bug test your code if using this feature! Often its easiest
        to create a new object and just swap the player over to
        that one instead.

        Arguments:
        new_typeclass (path/classobj) - type to switch to
        clean_attributes (bool/list) - will delete all attributes
                           stored on this object (but not any
                           of the database fields such as name or
                           location). You can't get attributes back,
                           but this is often the safest bet to make
                           sure nothing in the new typeclass clashes
                           with the old one. If you supply a list,
                           only those named attributes will be cleared.
        no_default - if this is active, the swapper will not allow for
                     swapping to a default typeclass in case the given
                     one fails for some reason. Instead the old one
                     will be preserved.
        Returns:
          boolean True/False depending on if the swap worked or not.

        """
        self.dbobj.swap_typeclass(new_typeclass,
                    clean_attributes=clean_attributes, no_default=no_default)

    def access(self, accessing_obj, access_type='read', default=False, **kwargs):
        """
        Determines if another object has permission to access this object
        in whatever way.

          accessing_obj (Object)- object trying to access this one
          access_type (string) - type of access sought
          default (bool) - what to return if no lock of access_type was found
          **kwargs - passed to the at_access hook along with the result.
        """
        result = self.dbobj.access(accessing_obj, access_type=access_type, default=default)
        self.at_access(result, accessing_obj, access_type, **kwargs)
        return result

    def check_permstring(self, permstring):
        """
        This explicitly checks the given string against this object's
        'permissions' property without involving any locks.

        permstring (string) - permission string that need to match a permission
                              on the object. (example: 'Builders')
        Note that this method does -not- call the at_access hook.
        """
        return self.dbobj.check_permstring(permstring)

    #
    # Hooks
    #

    def basetype_setup(self):
        """
        This is called when the object is first created as a database object
        reference and is executed before at_profession_object_creation(). This
        is usually not overridden.
        """
        # set up default locks
        pass

    def basetype_posthook_setup(self):
        """
        This is called after at_profession_object_creation() and will probably
        never be used.
        """
        pass

    def at_profession_object_creation(self):
        """
        This is called when the typeclassed object is created, and initial
        setup of the profession object should go here.
        """
        pass

    def at_profession_object_delete(self):
        """
        This is called before the profession object is deleted. This would be
        used to do some sort of cleanup.
        """
        pass

    def at_init(self):
        """
        This is called when the profession object is initialized. Currently
        this is not connected to anything.
        """
        pass

    def at_cmdset_get(self, **kwargs):
        """
        This is called when a cmdset is added to the profession object.
        """
        pass

    def at_pre_learn(self, character, proficiency):
        """
        This is called right before learning a proficiency in the profession
        object.
        """
        pass

    def at_post_learn(self, character, proficiency):
        """
        This is called right after learning a proficiency in the profession
        object.
        """
        pass

    def at_pre_forget(self, character, proficiency):
        """
        This is called right before forgetting a proficiency in the profession
        object.
        """
        pass

    def at_post_forget(self, character, proficiency):
        """
        This is called right after forgetting a proficiency in the profession
        object.
        """
        pass

class ProfessionBranch(Profession):
    pass

class ProfessionBox(Profession):
    pass

class Proficiency(Profession):
    pass
