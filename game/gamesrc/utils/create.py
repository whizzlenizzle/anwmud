"""
This module inherits from src.utils.create and then builds on top of it
with functionality to build additional models.

Models covered:
 ProfessionObjects
"""
from django.conf import settings

from src.utils.create import handle_dbref, create_object
from src.utils.utils import inherits_from, to_unicode
from src.utils.idmapper import SharedMemoryModel

__all__ = ("create_profession")

_ProfessionObject = None
_ProfessionObjectDB = None

_GA = object.__getattribute__

#
# Profession Object creation
#

def create_profession(typeclass=None, key=None, locks=None,
    permissions=None, locked=False, secret=False, category=None,
    description=None):
    """
    Create a new profession. Any profession object is a combination
    of a database object that stores data persistently to
    the database, and a typeclass, which on-the-fly 'decorates'
    the database object into whataver different type of object
    it is supposed to be.

    See game.gamesrc.professions.managers for methods to manipulate existing
    profession objects in the database.
    game.gamesrc.professions.profession_objects holds the base typeclasses and
    game.gamesrc.professions.models holds the database model.
    """
    global Profession, ProfessionDB
    if not Profession:
        from game.gamesrc.professions.profession_objects import ProfessionObject as Profession
    if not ProfessionDB:
        from game.gamesrc.professions.models import ProfessionObjectDB as ProfessionDB

    # input validation
    if not typeclass:
        typeclass = settings.BASE_PROFESSION_TYPECLASS
    elif isinstance(typeclass, ProfessionDB):
        # this is already a professionobjectdb instance, get typeclass
        typeclass = typeclass.typeclass.path
    elif isinstance(typeclass, Profession) or inherits_from(typeclass, Profession):
        # already a profession object typeclass, get path
        typeclass = typeclass.path
    typeclass = to_unicode(typeclass)

    locked = handle_dbref(locked, _ProfessionDB)
    secret = handle_dbref(secret, _ProfessionDB)
    category = handle_dbref(category, _ProfessionDB)
    description = handle_dbref(description, _ProfessionDB)

    new_db_object = _ProfessionDB(db_key=key, db_locked=locked,
        db_secret=secret, db_category=category, db_description=description,
        db_typeclass_path=typeclass)

    if not key:
        new_db_object.key = "#%i" % new_db_object.dbid

    new_object = new_db_object.typeclass

    # if not _GA(new_object, "is_typeclass")(typeclass, exact=True):
    #     try:
    #         SharedMemoryModel.delete(new_db_object)
    #     except AssertionError:
    #         pass
    # else:
    #     raise Exception(_GA(new_db_object, "typeclass_last_errmsg"))

    # from now on we can use the typeclass object
    # as if it was the database object.

    # call the hook methods. This is where all at_creation
    # customization happens as the typeclass stores custom
    # things on its database object.
    new_object.basetype_setup()
    new_object.at_profession_object_creation()
    new_object.basetype_posthook_setup()

    # we want the input to override that set in the hooks, so
    # we re-apply those if needed
    if new_object.key != key:
        new_object.key = key
    if new_object.locked != locked:
        new_object.locked = locked
    if new_object.secret != secret:
        new_object.secret = secret
    if new_object.category != category:
        new_object.category = category
    if new_object.description != description:
        new_object.description = description

    # permissions/locks overwrite hooks
    if permissions:
        new_object.permissions.add(permissions)
    if locks:
        new_object.locks.add(locks)

    return new_object

create_profession_object = create_profession

profession = create_profession
