from django.db import models

from django.conf import settings
from game.gamesrc.professions.manager import ProfessionManager

from src.commands.cmdsethandler import CmdSetHandler
from src.scripts.scripthandler import ScriptHandler
from src.typeclasses.models import TypedObject
from src.utils.utils import make_iter, lazy_property

__all__ = ("ProfessionDB")

_GA = object.__getattribute__
_SA = object.__setattr__
_DA = object.__delattr__

#
# ProfessionDB
#

class ProfessionDB(TypedObject):
    """
    The TypedObject supplies the following (inherited) properties:
        key - main name
        name - alias for key
        typeclass_path - the path to the decorating typeclass
        typeclass - auto-linked typeclass
        date_created - time stamp of object creation
        permissions - perm strings
        locks - lock definitions (handler)
        dbref - #id of object
        db - persistent attribute storage
        ndb - non-persistent attribute storage

    ProfessionDB adds the following properties:
        locked - true/false
        category - profession category
        secret - true/false if show in lists
        cmdset_storage - persistent cmdsets attached to object
        description - description of the profession object
    """

    #
    # ProfessionDB database model setup
    #

    db_locked = models.BooleanField(default=False, verbose_name="locked")
    db_category = models.TextField('category', blank=True, null=True)
    db_secret = models.BooleanField(default=False, verbose_name="locked")
    db_cmdset_storage = models.CharField('cmdset', max_length=255, null=True, blank=True, help_text="optional python path to a cmdset class.")
    db_description = models.TextField(null=True)

    # Database manager
    objects = ProfessionManager()

    # caches for typeclass loading
    _typeclass_paths = settings.PROFESSION_TYPECLASS_PATHS
    _default_typeclass_path = settings.BASE_PROFESSION_TYPECLASS or "game.gamesrc.professions.Profession"

    class Meta:
        "Define Django meta options"
        verbose_name = "Profession"

    # lazy-load handlers
    @lazy_property
    def cmdset(self):
        return CmdSetHandler(self, True)

    @lazy_property
    def scripts(self):
        return ScriptHandler(self)

    #
    # ProfessionDB class properties
    #

    def __cmdset_storage_get(self):
        """
        Getter. Allows for value = self.name.
        Returns a list of cmdset_storage.
        """
        storage = _GA(self, "db_cmdset_storage")
        # we need to check so storage is not None
        return [path.strip() for path in storage.split(',')] if storage else []
    #@cmdset_storage.setter
    def __cmdset_storage_set(self, value):
        """
        Setter. Allows for self.name = value.
        Stores as a comma-separated string.
        """
        _SA(self, "db_cmdset_storage", ",".join(str(val).strip() for val in make_iter(value)))
        _GA(self, "save")()
    #@cmdset_storage.deleter
    def __cmdset_storage_del(self):
        "Deleter. Allows for del self.name"
        _SA(self, "db_cmdset_storage", None)
        _GA(self, "save")()
    cmdset_storage = property(__cmdset_storage_get, __cmdset_storage_set, __cmdset_storage_del)
