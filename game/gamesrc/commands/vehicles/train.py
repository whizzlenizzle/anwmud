from ev import Command, CmdSet

class CmdEnterTrain(Command):
    """
    entering the train

    Usage:
      enter train

    This will embark on the train when
    it is in the same location. 
    """

    key = "enter train"
    locks = "cmd:not cmdinside()"

    def func(self):
        train = self.obj
        self.caller.msg("You board the train.")
        self.caller.move_to(train)


class CmdLeaveTrain(Command):
    """
    leaving the train 

    Usage:
      leave train

    This will exit the train into the 
    current location of the train.
    """

    key = "leave train"
    locks = "cmd:cmdinside()"

    def func(self):
        train = self.obj
        parent = train.location
        self.caller.move_to(parent)


class CmdSetTrain(CmdSet):

    def at_cmdset_creation(self):
        self.add(CmdEnterTrain())
        self.add(CmdLeaveTrain())