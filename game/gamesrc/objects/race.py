"""
Template for Races

How to make a new race:
    In this file or another (if another make sure to import this one), simply
    create another race class. For example: class OrcRace(Race)

    You can overload any of the methods found below. For anything unique about
    your race, such as any highly specific slots such as 'forehead' or
    'around the waist', or special features, it is recommended you utilize
    an overload of at_race_creation, which is always executed at the end of
    __init__, or perhaps even expand your new race with additional methods.

    This class is fairly barebones and defaults to a basic humanoid.

    After you have your new class, you simply need to instantiate it into
    the race attribute of whatever object you want to have this race.

    Example definition:
        self.db.race = OrcRace(self)

    You would then be able to call something like:
        character.race.limbs and so on.

Since the Race objects will not persist themselves in the database, but will
rather persist as an object within an attribute on many objects, it would be
wise to have a method on your Character/Mob/etc for "update_race" so you
can spread changes to all if needed, or create a Race handler.
"""

from django.conf import settings

class Race(object):
    """
    This is the base class for all races. As it is only a Python object,
    the races themselves will not persist in the database but are only defined
    in code. What race a Character holds will persist in the database under an
    attribute attached to that invidivual Character object. Characters obtain a
    race with the game.gamesrc.objects.Character become_race() method.

    The Race class provides the following properties:
        raceobj - the object that is this race

        name - the name of the race
        key - an alias for name, to remain consist with Evennia codebase
        limbs - what "limbs" this race has - more accurately considered
            hitboxes
        slots - equipment slots, generated from limbs and a few booleans
        stats - the initial stats for this race

    The Race class takes the following arguments:
        raceobj - the object that is this race
        name - the name of the race
        limbs - a list object of limbs (hit boxes), defaults to DEFAULT_LIMBS
        slots - a dictionary object made from limbs and booleans
        stats - a list (or whatever you make it) of stats (Stat class
            recommended)
        can_wear_boots - whether or not left/right feet are included in slots
            from DEFAULT_LIMBS, also...this will prevent the race from wearing
            shoes
        can_wear_earrings - whether or not the 'earrings' slot is added to
            slots and race can/cannot wear earrings
        can_wear_helmet - whether or not head slot is included from
            DEFAULT_LIMBS, and will bar the object from wearing a helmet
        can_wear_necklace - whether or not 'necklace' is added to slots and
            race can/cannot wear a necklace
        can_wear_mask - whether or not 'face' is added to the slots
        has_fingers - whether or not this race has fingers. if true,
            DEFAULT_LIMBS has two hands, and each has 4 fingers and 1 thumb.

    The Race class has the following methods:
        set_key - set the key/name
        set_name - set the key/name
        at_race_creation - called after __init__ is done. overload this for
            anything special about the race you create.
    """

    def __init__(self, raceobj, name=None, limbs=settings.DEFAULT_LIMBS,
        slots=None, stats=None, can_wear_boots=True, can_wear_helmet=True,
        can_wear_necklace=True, can_wear_mask=True, can_wear_earrings=True,
        has_fingers=True):
        """
        This method takes only one mandatory argument of raceobj, which should
        be the object that is becoming this race. This is how the race would
        then be able to modify attributes of its host object when applied.
        """
        self.raceobj = raceobj
        self.name = name
        self.limbs = limbs

        self.can_wear_boots = can_wear_boots
        self.can_wear_earrings = can_wear_earrings
        self.can_wear_helmet = can_wear_helmet
        self.can_wear_mask = can_wear_mask
        self.can_wear_necklace = can_wear_necklace
        self.has_fingers = has_fingers

        if slots:
            self.slots = slots
        else:
            tmp = []
            if self.limbs == settings.DEFAULT_LIMBS:
                self.slots = dict()
                for limb in self.limbs:
                    tmp.append(limb)
                if self.can_wear_mask:
                    tmp.append('face')
                if self.can_wear_necklace:
                    tmp.append('necklace')
                if self.can_wear_boots == False:
                    for slot in tmp:
                        if 'feet' in slot:
                            tmp.remove(slot)
                if self.can_wear_earrings:
                    for slot in ['right ear', 'left ear']:
                        tmp.append(slot)
                if self.can_wear_helmet == False:
                    tmp.remove('head')

                for slot in range(len(tmp)):
                    self.slots[tmp[slot]] = None

                if self.has_fingers:
                    self.slots['right hand'] = {}
                    self.slots['left hand'] = {}
                    fingers = ['index finger', 'middle finger', 'ring finger',\
                    'pinky finger', 'thumb']

                    for hand in ['left hand', 'right hand']:
                        for finger in fingers:
                            self.slots[hand][finger] = None
            else:
                # if provided with custom limbs, but not custom slots, default
                # to simply having slots for each limb provided
                for limb in self.limbs:
                    tmp.append(limb)
                for slot in range(len(tmp)):
                    self.slots[tmp[slot]] = None
            del tmp

        # for stats, you should use game.gamesrc.utils.helpers.Stat
        if stats:
            self.stats = stats
        else:
            self.stats = []

        self.at_race_creation()

    @property
    def key(self):
        return self.name

    def set_key(self, key=None):
        self.name = key

    def set_name(self, name=None):
        self.name = name

    def at_race_creation(self):
        """
        This method should be overloaded by new race classes for setup, rather
        than changing __init__ if the changes are minor (limbs, slots, etc).
        """
        pass
