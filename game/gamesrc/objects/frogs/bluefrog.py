"""
Blue Frog

This is an object placed in a room for testing Character features. It
provides open permissions on administrative actions against Character
objects, such as changing race, professions, adding/removing skills and
so on.

This should never be created in a public area.
"""

from ev import Object

from game.gamesrc.commands.frogs.cmdset import BlueFrogCmdSet

class BlueFrog(Object):
    def at_object_creation(self):
        self.cmdset.add_default(BlueFrogCmdSet, permanent=True)
