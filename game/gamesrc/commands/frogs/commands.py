"""
Commands for the Blue Frog CmdSet
"""

# class CmdNudge(Command):
#     """
#     Try to nudge the button's lid

#     Usage:
#       nudge lid

#     This command will have you try to
#     push the lid of the button away.
#     """

#     key = "nudge lid"  # two-word command name!
#     aliases = ["nudge"]
#     locks = "cmd:all()"

#     def func(self):
#         """
#         nudge the lid. Random chance of success to open it.
#         """
#         rand = random.random()
#         if rand < 0.5:
#             self.caller.msg("You nudge at the lid. It seems stuck.")
#         elif 0.5 <= rand < 0.7:
#             self.caller.msg("You move the lid back and forth. It won't budge.")
#         else:
#             self.caller.msg("You manage to get a nail under the lid.")
#             self.caller.execute_cmd("open lid")

from ev import Command

class CmdBlueFrogLook(Command):
    """
    The look while next to a blue frog

    Usage:
        look
    """

    key = "look"
    aliases = ["l", "ls"]
    locks = "cmd:all()"

    def func(self):
        """
        Look in the presence of the Blue Frog
        """

        line = "*"*80
        self.caller.msg(line)
        self.caller.msg("\nThis is a Blue Frog.\n")
        self.caller.msg(line)





