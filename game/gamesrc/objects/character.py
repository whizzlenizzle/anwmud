"""
Template for Characters

Copy this module up one level and name it as you like, then
use it as a template to create your own Character class.

To make new logins default to creating characters
of your new type, change settings.BASE_CHARACTER_TYPECLASS to point to
your new class, e.g.

settings.BASE_CHARACTER_TYPECLASS = "game.gamesrc.objects.mychar.MyChar"

Note that objects already created in the database will not notice
this change, you have to convert them manually e.g. with the
@typeclass command.

"""

from django.conf import settings

from game.gamesrc.objects.mobile import Mobile
from game.gamesrc.objects.race import Race
from game.gamesrc.utils.stat import Stat

STATUS_LIST = ['{gPlayable{n', '{gOn Hold{n', '{yPending{n', '{rArchived{n']
STATUS_PLAYABLE = 0
STATUS_ON_HOLD = 1
STATUS_PENDING = 2
STATUS_ARCHIVED = 3

class Character(Mobile):
    """
    The Character object is drawn from Mobile and apart from what is
    overloaded and defined below, will operate exactly the same. See
    Mobile in game.gamesrc.objects.mobile.Mobile
    """
    def at_object_creation(self):
        self.cmdset.add_default(settings.CMDSET_CHARACTER, permanent=True)
        self.db.status = STATUS_PENDING

    def at_post_puppet(self):
        super(Character, self).at_post_puppet()
        self.execute_cmd("look")
        if self.location:
            self.location.msg_contents("%s has entered the game." % self.name, exclude=[self])

    def at_post_unpuppet(self, player, sessid=None):
        """
        We stove away the character when the player goes ooc/logs off,
        otherwise the character object will remain in the room also after the
        player logged off ("headless", so to say).
        """
        if self.location: # have to check, in case of multiple connections closing
            self.location.msg_contents("%s has left the game." % self.name, exclude=[self])
            self.db.prelogout_location = self.location
            self.location = None

