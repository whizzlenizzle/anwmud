from django.contrib.contenttypes.models import ContentType

from src.utils.search import *

__all__ = ("search_profession_object_tag")

ProfessionObjectDB = ContentType.objects.get(app_label="professions", model="professionobjectdb").model_class()

def search_profession_object_tag(key=None, category=None):
    return ProfessionObjectDB.get_by_tag(key=key, category=category)
search_profobj_tag = search_profession_object_tag
